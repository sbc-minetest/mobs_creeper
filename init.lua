
-- Creeper by Davedevils (from his subgame MineClone)

mobs:register_mob("mobs_creeper:creeper", {
	type = "monster",
	passive = false,
	attack_type = "explode",
	damage = 40,
	hp_min = 20,
	hp_max = 20,
	armor = 100,
  attack_players = true,
  attack_npcs= true,
	explosion_radius = 1,
	explosion_timer = 1.5,
  explosion_damage_radius = 7,
	reach = 4,
  runaway_from = {"mobs_animal:kitten"},
	pathfinding = 1,
	collisionbox = {-0.4, -1, -0.4, 0.4, 0.8, 0.4},
	visual = "mesh",
	mesh = "mobs_tree_monster.b3d",
	drawtype = "front",
	textures = {
		{"mobs_creeper.png"},
	},
	visual_size = {x=1,y=1},
	blood_texture = "mobs_creeper_inv.png",
	makes_footstep_sound = true,
	sounds = {
--		random = "mobs_treemonster",
    fuse = "tnt_ignite",
		explode = "tnt_explode",
    distance = 16,
	},
	walk_velocity = 1,
	run_velocity = 2,
	view_range = 16,
  drops = {
    {name = "tnt:gunpowder",
    chance = 1,
    min = 0,
    max = 2,},
  },
	water_damage = 1,
	lava_damage = 4,
	light_damage = 0,
  float = 1,
  fear_height = 4,
	animation = {
		stand_start = 0,		stand_end = 24,
		walk_start = 25,		walk_end = 47,	
		run_start = 48,			run_end = 62,
		punch_start = 48,		punch_end = 62,
		speed_normal = 15,		speed_run = 15,
	},
})
--mobs:register_spawn("mobs_creeper:creeper", {"default:dirt_with_grass"}, 20, 8, 12000, 1, 31000)

mobs:spawn({
	name = "mobs_creeper:creeper",
	nodes = {"default:dirt_with_grass"},
	min_light = 0,
	max_light = 7,
	chance = 16500,
	active_object_count = 2,
	min_height = 0,
	day_toggle = false,
})
mobs:register_egg("mobs_creeper:creeper", "Creeper", "mobs_creeper_inv.png", 1)
